
const{sign}=require('jsonwebtoken')
const jwt=require('jsonwebtoken')
const express = require('express')
const bodyParser = require("body-parser")
const mongoose = require('mongoose')
require('dotenv');
const Product = require('./Product')

const url = 'mongodb://localhost/mydb'
const app = express()

app.use(express.json())
//connect to database
mongoose.connect(url, { useNewUrlParser: true })

const conObject = mongoose.connection

conObject.on('open', () => {
      console.log('connected...')
})
app.use(bodyParser.urlencoded({ extended: true }))
app.get('/', verifyuser,(req, res) => {
      try {
            Product.find().then((products, err) => {
                  if (err) {
                        res.status(400);
                        return res.send(err);
                  }
                  else if(products.length==0){
                        res.status(400);
                        res.send("No product found");
                  }
                  else {
                        return res.json(products);
                  }
            })
      }
      catch(err){
            console.log("error=",err);
      }
});

app.post('/post',verifyuser,(req,res)=>{

      jwt.verify(req.token,'secretkey',(err,authdata)=>{
            if (err){
                  res.sendStatus(403)
            }else{
                  res.json({
                        authdata
                  })
                  const product=new Product({
                        product:req.body.product,
                        price:req.body.price
                      });
                  product.save().then((product,err)=>{
                        if(err){
                              res.send("error occured");
                            }
                        else{
                              res.json(product);
                            }
                  })

            }
      })
});
app.post('/api/login',(req,res)=>{
      //user
      const user={
            id:1,
            username:'mvp',
            email:"palaskarm4@gmail.com"
      }

      jwt.sign({user},'secretkey',(err,token)=>{
         res.json({token});
      })
});

// verfying user
function verifyuser(req,res,next){
      
      //getting authorization header value
      const bearerheader=req.headers['authorization']
      //checking if header value if undefined 
      if(typeof(bearerheader!='undefined')){
          //split at the space
            const bearer=bearerheader.split(' ');
         // getting the token from array
         const token=bearer[1];
         //setting the token
         req.token=token;
         next();

      } else{
            res.sendstatus(403)
      }
}


const port = 3003;


app.listen(port, 'localhost', () => {
      console.log(`server started on port: ${port}`)
})


